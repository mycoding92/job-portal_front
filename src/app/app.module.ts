import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list';
import { ListJobsComponent } from './list-jobs/list-jobs.component';

@NgModule({
  declarations: [AppComponent, ListJobsComponent],
  imports: [BrowserModule, BrowserAnimationsModule, MatListModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
